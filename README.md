# Efficient Logger for Your Flutter App

<br>

![Example of logger output.](logger-output-example.png)

<p style="text-align: center;"><i> Example of logger output. </i></p>

<br>

In this article, I show you how to set up a simple and efficient logger for your
 app.

The logger presented here has the following characteristics:

- Easily customizable for your needs and taste;
- Option to log both to terminal and to DevTools, or only to DevTools;
- Colored terminal output;
- Minimum code for logger setup;
- Most log calls within one line of code;
- Makes great use of code autocompletion to minimize the number of keystrokes
 needed;
- Integrated option for timed logs; and
- Integrated option to log widget builds.

In the following sections, I will show you some examples of how to use the
 logger.


## Template Project

To run the example shown in this article, I created a new project using the
 Flutter's skeleton template. To implement it as shown here, you can create a
 new project by running:

```
flutter create -t skeleton --project-name my_app_name app-name
```
> Where `my_app_name` will be the name of your project, and `app-name` the name
 of the folder in which your app will be created (a new folder will be created
 if non-existing).


## Logger Setup

First of all, there is no need to import any third-party packages if you are
 already using the [Internationalization (intl)](https://pub.dev/packages/intl)
 package provided by the Dart team.

In case you don't already use the Dart's [intl](https://pub.dev/packages/intl)
 package, you can add it to your project by running this command in your
 project's directory:

```
$ flutter pub add intl
```

And this will automatically add the line below to your project's `pubspec.yaml`
 file, under the dependencies section:

```
dependencies:
  (...)
  intl: ^0.17.0 # new line
```


## New File

Create a new file named `logger.dart` in your project's `lib` directory, i.e. in
 the same folder where the `main.dart` file is located.

You can copy the file contents directly from this
 [file](https://gitlab.com/rodweber/logger-example/-/blob/main/lib/logger.dart)
 in the linked repository, or from the last section of this article. The logger
 was conceived to be easily understood.


## Usage

To start using the logger, it is as simple as adding these lines to your
 `main.dart` file: 

```dart
// Other imports here...

// Logger:
import 'package:my_app_name/logger.dart';

// Log header:
const String _h = 'main';

void main() async {
  // (...)
```
> Where `my_app_name` is the name of your project, as chosen in the template
 project section.

Then, still in your `main.dart` file, you can set the log level as shown below:

```dart
void main() async {
  // Set log level to show.
  // Can be set only once. No need to configure it anywhere else in your app.
  Logger().setLogLevel(LogLevel.debugFinest);

  // (...)
```

The logging options are as follows, in order of importance or severity:

- `logDebugFinest()`
- `logDebugFiner()`
- `logDebugFine()`
- `logDebug()`
- `logInfo()`
- `logSuccess()`
- `logWarning()`
- `logError()`
- `logFatal()`

In addition, there are options for timed logs. These will be shown below.

For arbitrary blocks of code, you can use:

- `logTimerStart()`
- `logTimerStop()`

As an example for your app initialization, you can do:

```dart
void main() async {
  Logger().setLogLevel(LogLevel.debugFinest);
  
  final timer0 = logTimerStart(_h, 'Starting...', level: LogLevel.info);
  
  // Initialization code here...
  
  logTimerStop(_h, timer0, 'Initialization completed', level: LogLevel.success);
  
  // runApp...
}
```

An example of awaiting an async operation to complete and logging the time
 spent:

```dart
  final timer1 = logTimerStart(_h, 'Loading settings...');
  // This settingsController call can be found in Flutter's skeleton template.
  final settingsController = SettingsController(SettingsService());
  await settingsController.loadSettings();
  logTimerStop(_h, timer1, 'Settings loaded');
```
> `logTimerStop()` appends the time spent (in milliseconds) to the end of the
 log message.

An example of calling an expensive async operation and not awaiting it to
 finish:

```dart
  final timer2 = logTimerStart(_h, 'Starting an expensive async operation...');
  Future.delayed( // an async method here
    const Duration(seconds: 2), // and a task that takes long to complete
    () => logTimerStop(_h, timer2, 'Completed the expensive async operation'),
  );
  logDebugFinest(_h, 'This line runs before the expensive async completes');
```

An example of calling an expensive async operation that may fail:

```dart
void main() async {
  Logger().setLogLevel(LogLevel.debugFinest);
  
  // (...)
  
  logDebugFinest(_h, 'Starting a second expensive async operation...');
  fetchUserData().then((result) {
    logDebugFine(_h, 'Got the result: $result');
  }).catchError((error) {
    logErrorObject(_h, error, 'Caught an error in the async operation!');
  });
  
  // (...)
}

Future<Object> fetchUserData() {
  // Imagine that this function is fetching user info but encounters a bug.
  return Future.delayed(
    const Duration(seconds: 3),
    () => throw Exception('Logout failed: user ID is invalid'),
  );
}
```

Another timed log option is to log a widget's build method by using:

- `logBuild()`

As an example of logging a build method, we can update the `lib/src/app.dart`
 file as follows:

```dart
// Other imports here...

// Logger:
import 'package:my_app_name/logger.dart';

// Log header:
const String _h = 'app';

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.settingsController}) : super(key: key);

  final SettingsController settingsController;

  @override
  Widget build(BuildContext context) {
  
    logBuild(_h); // just this!
    
    return AnimatedBuilder(
    // (...)
```

You can find the implementation of all these examples in
 [this repository](https://gitlab.com/rodweber/logger-example).

The complete `main.dart` file with all examples looks as follows:

```dart
import 'package:flutter/material.dart';

import 'src/app.dart';
import 'src/settings/settings_controller.dart';
import 'src/settings/settings_service.dart';

// Logger:
import 'package:my_app_name/logger.dart';

// Log header:
const String _h = 'main';

void main() async {
  // Set log level to show.
  // Can be set only once. No need to configure it anywhere else in your app.
  Logger().setLogLevel(LogLevel.debugFinest);

  // Example: log time for app initialization (part 1/2).
  final timer0 = logTimerStart(_h, 'Starting...', level: LogLevel.info);

  // Example: Fibonacci function.
  logDebug(_h, 'Fibonacci(4) is: ${fibonacci(4)}');

  Logger().setLogLevel(LogLevel.debug); // skip log levels lower than debug.
  logDebug(_h, 'Fibonacci(5) is: ${fibonacci(5)}');

  logDebug(_h, 'Fibonacci(-42) is: ${fibonacci(-42)}');

  Logger().setLogLevel(LogLevel.debugFinest); // show all logs again.

  // Example: await an async operation to complete and log the time spent.
  final timer1 = logTimerStart(_h, 'Loading settings...');
  final settingsController = SettingsController(SettingsService());
  await settingsController.loadSettings();
  logTimerStop(_h, timer1, 'Settings loaded');

  // Example: call an expensive async operation and do not await it to finish.
  final timer2 = logTimerStart(_h, 'Starting an expensive async operation...');
  Future.delayed(
    const Duration(seconds: 2),
    () => logTimerStop(_h, timer2, 'Completed the expensive async operation'),
  );
  logDebugFinest(_h, 'This line runs before the expensive async completes');

  // Example: call an expensive async operation that may fail; do not await it.
  logDebugFinest(_h, 'Starting a second expensive async operation...');
  fetchUserData().then((result) {
    logDebugFine(_h, 'Got the result: $result');
  }).catchError((error) {
    logErrorObject(_h, error, 'Caught an error in the async operation!');
  });

  // Example: log time for app initialization (part 2/2).
  logTimerStop(_h, timer0, 'Initialization completed', level: LogLevel.success);

  // Run the app and pass in the SettingsController. The app listens to the
  // SettingsController for changes, then passes it further down to the
  // SettingsView.
  runApp(MyApp(settingsController: settingsController));
}

int fibonacci(int n) {
  if (n <= 2) {
    if (n < 0) logError(_h, 'Unexpected negative n: $n');
    return 1;
  } else {
    logDebugFinest(_h, 'recursion: n = $n');
    return fibonacci(n - 2) + fibonacci(n - 1);
  }
}

Future<Object> fetchUserData() {
  // Imagine that this function is fetching user info but encounters a bug.
  return Future.delayed(
    const Duration(seconds: 3),
    () => throw Exception('Logout failed: user ID is invalid'),
  );
}
```


## Logger

File `lib/logger.dart`:

```dart
/// Dart and Flutter packages.
import 'dart:developer' as dart;
import 'dart:async' show Zone;
import 'package:flutter/foundation.dart' as flutter;
import 'package:flutter/scheduler.dart' show SchedulerBinding;
import 'package:intl/intl.dart' show NumberFormat;

/// Number formatter.
final _formatter = NumberFormat('#,##0.0', "en_US");

/// Supported log levels.
enum LogLevel {
  debugFinest,
  debugFiner,
  debugFine,
  debug,
  info,
  success,
  warning,
  error,
  fatal,
}

/// Value of each log level.
const Map<LogLevel, int> _number = {
  LogLevel.debugFinest: 100,
  LogLevel.debugFiner: 200,
  LogLevel.debugFine: 300,
  LogLevel.debug: 400,
  LogLevel.info: 500,
  LogLevel.success: 500,
  LogLevel.warning: 750,
  LogLevel.error: 1000,
  LogLevel.fatal: 2000,
};

/// Output color for each log level.
Map<LogLevel, String> _color = {
  LogLevel.debugFinest: _ansi[_Color.darkGray]!,
  LogLevel.debugFiner: _ansi[_Color.lightGray]!,
  LogLevel.debugFine: _ansi[_Color.white]!,
  LogLevel.debug: _ansi[_Color.defaultForeground]!,
  LogLevel.info: _ansi[_Color.cyan]!,
  LogLevel.success: _ansi[_Color.green]!,
  LogLevel.warning: _ansi[_Color.yellow]!,
  LogLevel.error: _ansi[_Color.red]!,
  LogLevel.fatal: _ansi[_Color.redBold]!,
};

/// Header symbol for each log level.
const Map<LogLevel, String> _symbol = {
  LogLevel.debugFinest: '\u2192', // Rightwards arrow.
  LogLevel.debugFiner: '\u2192', // Rightwards arrow.
  LogLevel.debugFine: '\u2192', // Rightwards arrow.
  LogLevel.debug: '\u2192', // Rightwards arrow.
  LogLevel.info: '\u2139', // Information source.
  LogLevel.success: '\u2713', // Check mark.
  LogLevel.warning: '!', // Exclamation point.
  LogLevel.error: '\u2718', // Heavy ballot X.
  LogLevel.fatal: '\u2718', // Heavy ballot X.
};

/// Supported ANSI colors.
enum _Color {
  defaultForeground,
  black,
  red,
  redBold,
  green,
  yellow,
  purple,
  magenta,
  cyan,
  lightGray,
  lightGrayBold,
  darkGray,
  darkGrayBold,
  lightRed,
  lightGreen,
  lightYellow,
  lightPurple,
  lightMagenta,
  lightCyan,
  white,
  whiteBold,
  reset,
}

/// ANSI escape codes.
const Map<_Color, String> _ansi = {
  _Color.defaultForeground: '\x1B[39m',
  _Color.black: '\x1B[30m',
  _Color.red: '\x1B[31m',
  _Color.redBold: '\x1B[31;1m',
  _Color.green: '\x1B[32m',
  _Color.yellow: '\x1B[33m',
  _Color.purple: '\x1B[34m',
  _Color.magenta: '\x1B[35m',
  _Color.cyan: '\x1B[36m',
  _Color.lightGray: '\x1B[37m',
  _Color.lightGrayBold: '\x1B[37;1m',
  _Color.darkGray: '\x1B[90m',
  _Color.darkGrayBold: '\x1B[90;1m',
  _Color.lightRed: '\x1B[91m',
  _Color.lightGreen: '\x1B[92m',
  _Color.lightYellow: '\x1B[93m',
  _Color.lightPurple: '\x1B[94m',
  _Color.lightMagenta: '\x1B[95m',
  _Color.lightCyan: '\x1B[96m',
  _Color.white: '\x1B[97m',
  _Color.whiteBold: '\x1B[97;1m',
  _Color.reset: '\x1B[0m',
};

/// Logger singleton.
class Logger {
  /// Minimum log level to show. Defaults to `LogLevel.debugFinest`.
  LogLevel _minLogLevel = LogLevel.debugFinest;

  /// Header width.
  int _headerWidth = 15;

  /// Header right-alignment.
  bool _headerRightAlign = true;

  /// Option to log to DevTools only.
  bool _logDevToolsOnly = false;

  /// Singleton which is returned every time this class is called.
  static final Logger _singleton = Logger._create();

  /// Private constructor. Called once to instantiate the singleton object.
  Logger._create();

  /// Public constructor. Returns a reference to the singleton object.
  factory Logger() {
    return _singleton;
  }

  /// Get minimum log level to show.
  LogLevel get logLevel => _minLogLevel;

  /// Set minimum log level to show.
  void setLogLevel(LogLevel level) => _minLogLevel = level;

  /// Get width of the full header string.
  int get headerWidth => _headerWidth;

  /// Set width of the full header string.
  void setHeaderWidth(int width) => _headerWidth = width;

  /// Get header alignment.
  bool get headerRightAlign => _headerRightAlign;

  /// Set header alignment.
  void setHeaderRightAlign(bool rightAlign) => _headerRightAlign = rightAlign;

  /// Get option to log to DevTools only.
  bool get logDevToolsOnly => _logDevToolsOnly;

  /// Set option to log to DevTools only.
  void setLogDevToolsOnly(bool option) => _logDevToolsOnly = option;
}

/// Reference to the [Logger] singleton.
Logger _logger = Logger();

/// Private log method that returns immediately when in release mode.
void __log(
  String? header,
  String message,
  LogLevel level, {
  Object? object,
}) {
  // Break if in release mode:
  if (flutter.kReleaseMode) return;
  // Break if this message level is lower than minimum level to show:
  if (_number[level]! < _number[_logger.logLevel]!) return;
  // Otherwise, log the message to DevTools if option has been set:
  if (_logger.logDevToolsOnly) {
    dart.log(
      message,
      time: null,
      sequenceNumber: null,
      level: _number[level]!,
      name: header ?? '',
      zone: null,
      error: object,
      stackTrace: null,
    );
    // And break out of function to not print to terminal:
    return;
  }
  // From here on, print to terminal and to DevTools:
  String _headerFull = '';
  if (header != null) {
    if (_logger.headerRightAlign) {
      _headerFull =
          _symbol[level]! + ' ' + header.padLeft(_logger.headerWidth) + ' : ';
    } else {
      _headerFull =
          _symbol[level]! + ' ' + header.padRight(_logger.headerWidth) + ' : ';
    }
  } else {
    _headerFull =
        _symbol[level]! + ' ' + ' '.padLeft(_logger.headerWidth) + ' : ';
  }
  flutter.debugPrint(_color[level]! + _headerFull + message + '\x1B[0m');
  if (object != null) {
    // If log call passed an object to print:
    try {
      // Try to print object.
      flutter.debugPrint(_color[level]! + object.toString() + '\x1B[0m');
    } catch (error) {
      // Otherwise, print error.
      flutter.debugPrint(_color[level]! + error.toString() + '\x1B[0m');
    }
  }
}

/// Implements the behavior of the log function from the dart:developer package.
void log(
  String message, {
  DateTime? time,
  int? sequenceNumber,
  int level = 0,
  String name = '',
  Zone? zone,
  Object? error,
  StackTrace? stackTrace,
}) {
  dart.log(
    message,
    time: time,
    sequenceNumber: sequenceNumber,
    level: level,
    name: name,
    zone: zone,
    error: error,
    stackTrace: stackTrace,
  );
}

void logDebugFinest(String _h, String message) {
  __log(_h, message, LogLevel.debugFinest);
}

void logDebugFiner(String _h, String message) {
  __log(_h, message, LogLevel.debugFiner);
}

void logDebugFine(String _h, String message) {
  __log(_h, message, LogLevel.debugFine);
}

void logDebug(String _h, String message) {
  __log(_h, message, LogLevel.debug);
}

void logInfo(String _h, String message) {
  __log(_h, message, LogLevel.info);
}

void logSuccess(String _h, String message) {
  __log(_h, message, LogLevel.success);
}

void logWarning(String _h, String message) {
  __log(_h, message, LogLevel.warning);
}

void logError(String _h, String message) {
  __log(_h, message, LogLevel.error);
}

void logErrorObject(String _h, Object error, String message) {
  __log(_h, message, LogLevel.error, object: error);
}

void logFatal(String _h, Object? error, String message) {
  __log(_h, message, LogLevel.fatal, object: error);
}

void logBuild(String _h, {LogLevel level = LogLevel.debugFinest}) {
  final timer = logTimerStart(_h, 'Building...', level: level);
  SchedulerBinding.instance?.addPostFrameCallback(
    (_) => logTimerStop(_h, timer, 'Built', level: level),
  );
}

Stopwatch logTimerStart(
  String _h,
  String message, {
  LogLevel level = LogLevel.debugFinest,
}) {
  Stopwatch timer = Stopwatch()..start();
  __log(_h, message, level);
  return timer;
}

void logTimerStop(
  String _h,
  Stopwatch timer,
  String message, {
  LogLevel level = LogLevel.debugFiner,
}) {
  timer.stop();
  final _timerMilliseconds = _formatter.format(timer.elapsedMilliseconds);
  message = message + ' in ${_timerMilliseconds}ms';
  __log(_h, message, level);
}
```

That's it for now. I hope you can make good use of it!
